module test.mixed;

import hunt.container.List;


public interface FoodRepository {
	List<Food> getFood();
}
